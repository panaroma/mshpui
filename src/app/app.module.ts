import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CalculationDetailsComponent } from './calculation-details/calculation-details.component';
import {HttpClientModule} from '@angular/common/http';
import {ApiService} from './api.service';

@NgModule({
  declarations: [
    AppComponent,
    CalculationDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }

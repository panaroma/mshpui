import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Weathercalc} from './weathercalc.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiURL: string = '/datapoint/calcdata';

  constructor(private httpClient: HttpClient) { }

   // Fetching weather data using HttpClient API get() method

  getWeatherCalculation() {
    return this.httpClient.get<Weathercalc>(this.apiURL);
  }

}

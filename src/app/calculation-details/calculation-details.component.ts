import { Component, OnInit } from '@angular/core';
import {Weathercalc} from '../weathercalc.model';
import {ApiService} from '../api.service';

@Component({
  selector: 'app-calculation-details',
  templateUrl: './calculation-details.component.html',
  styleUrls: ['./calculation-details.component.css']
})
export class CalculationDetailsComponent implements OnInit {

  weathercalc$: Weathercalc;

    constructor(private apiService: ApiService) { }

  ngOnInit() {

       this.apiService.getWeatherCalculation().subscribe( data => {
         this.weathercalc$ = data;
       });
  }

}

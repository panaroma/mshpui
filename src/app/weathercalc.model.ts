

export class Weathercalc {

  noOfStation: number;
  noOfSensorError: number;
  avgValueOfH: number;
  avgValueOfP: number;
  avgValueOfT: number;
  maxValueOfH: number;
  maxValueOfP: number;
  maxValueOfT: number;
  minValueOfH: number;
  minValueOfP: number;
  minValueOfT: number;

}

